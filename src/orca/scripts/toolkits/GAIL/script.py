# Orca
#
# Copyright (C) 2013-2014 Igalia, S.L.
#
# Author: Joanmarie Diggs <jdiggs@igalia.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., Franklin Street, Fifth Floor,
# Boston MA  02110-1301 USA.

__id__        = "$Id$"
__version__   = "$Revision$"
__date__      = "$Date$"
__copyright__ = "Copyright (c) 2013-2014 Igalia, S.L."
__license__   = "LGPL"

import orca.debug as debug
import orca.focus_manager as focus_manager
import orca.scripts.default as default
from orca.ax_object import AXObject
from orca.ax_utilities import AXUtilities
from .script_utilities import Utilities

_focusManager = focus_manager.getManager()

class Script(default.Script):

    def __init__(self, app):
        default.Script.__init__(self, app)

    def getUtilities(self):
        return Utilities(self)

    def locusOfFocusChanged(self, event, oldFocus, newFocus):
        """Handles changes of focus of interest to the script."""

        if self.utilities.isInOpenMenuBarMenu(newFocus):
            window = self.utilities.topLevelObject(newFocus)
            if window and _focusManager.get_active_window() != window:
                _focusManager.set_active_window(window)

        super().locusOfFocusChanged(event, oldFocus, newFocus)

    def onActiveDescendantChanged(self, event):
        """Callback for object:active-descendant-changed accessibility events."""

        if not self.utilities.isTypeahead(_focusManager.get_locus_of_focus()):
            super().onActiveDescendantChanged(event)
            return

        msg = "GAIL: locusOfFocus believed to be typeahead. Presenting change."
        debug.printMessage(debug.LEVEL_INFO, msg, True)
        self.presentObject(event.any_data, interrupt=True)

    def onFocus(self, event):
        """Callback for focus: accessibility events."""

        # NOTE: This event type is deprecated and Orca should no longer use it.
        # This callback remains just to handle bugs in applications and toolkits
        # that fail to reliably emit object:state-changed:focused events.
        if self.utilities.isLayoutOnly(event.source):
            return

        focus = _focusManager.get_locus_of_focus()
        if self.utilities.isTypeahead(focus) \
           and AXObject.supports_table(event.source) \
           and not AXUtilities.is_focused(event.source):
            return

        ancestor = AXObject.find_ancestor(focus, lambda x: x == event.source)
        if not ancestor:
            _focusManager.set_locus_of_focus(event, event.source)
            return

        if AXObject.supports_table(ancestor):
            return

        if AXUtilities.is_menu(ancestor) \
           and not AXObject.find_ancestor(ancestor, AXUtilities.is_menu):
            return

        _focusManager.set_locus_of_focus(event, event.source)

    def onSelectionChanged(self, event):
        """Callback for object:selection-changed accessibility events."""

        if not AXUtilities.is_focused(event.source) \
           and self.utilities.isTypeahead(_focusManager.get_locus_of_focus()):
            msg = "GAIL: locusOfFocus believed to be typeahead. Presenting change."
            debug.printMessage(debug.LEVEL_INFO, msg, True)

            selectedChildren = self.utilities.selectedChildren(event.source)
            for child in selectedChildren:
                if not self.utilities.isLayoutOnly(child):
                    self.presentObject(child)
            return

        if AXUtilities.is_layered_pane(event.source) \
           and self.utilities.selectedChildCount(event.source) > 1:
            return

        super().onSelectionChanged(event)

    def onTextSelectionChanged(self, event):
        """Callback for object:text-selection-changed accessibility events."""

        if not self.utilities.isSameObject(event.source, _focusManager.get_locus_of_focus()):
            return

        default.Script.onTextSelectionChanged(self, event)
